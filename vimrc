" Note: Skip initialization for vim-tiny or vim-small.
if !1 | finish | endif

if has('vim_starting')
  if &compatible
    set nocompatible               " Be iMproved
  endif

  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:
" Refer to |:NeoBundle-examples|.
" Note: You don't set neobundle setting in .gvimrc!
NeoBundle 'kien/ctrlp.vim'
NeoBundle 'pangloss/vim-javascript'
NeoBundle 'mustache/vim-mustache-handlebars'
NeoBundle 'christoomey/vim-tmux-navigator'
NeoBundle 'dkprice/vim-easygrep'
NeoBundle 'Valloric/YouCompleteMe'
NeoBundle 'groenewege/vim-less'
NeoBundle 'scrooloose/nerdtree'
call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck


syntax on
:inoremap jk <Esc>

set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

set autoindent
set smartindent

set number
set noswapfile
set nobackup

let mapleader=" "

nnoremap <Leader>ff :CtrlP<CR>

set wildignore+=*.swp,*.bak,*.pyc,*.class,*/tmp/**,*/dist/**,*/node_modules/**,*/bower_components/**  " wildmenu: ignore these extensions

" always show what file you're on
set laststatus=2
set statusline+=%f

" reload files from disk if they've changed
set autoread

" vim-less
nnoremap <Leader>m :w <BAR> !lessc % > %:t:r.css<CR><space>

" open nerdtree
map <Leader>d :NERDTreeToggle<CR>
nmap <Leader>nt :NERDTreeFind<CR>

#!/bin/bash

sudo rm -rf ~/.tmux > /dev/null 2>&1
sudo rm -rf ~/.vimrc > /dev/null 2>&1
sudo rm -rf ~/.viminfo > /dev/null 2>&1
sudo rm -rf ~/.tmux.conf > /dev/null 2>&1

ln -s ~/super-basic-dotfilez/vimrc ~/.vimrc
ln -s ~/super-basic-dotfilez/tmux ~/.tmux
ln -s ~/super-basic-dotfilez/tmux.conf ~/.tmux.conf


echo "*******************************"
echo "*    Restart your terminal    *"
echo "*******************************"
